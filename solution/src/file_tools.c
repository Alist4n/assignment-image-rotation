#include "file_tools.h"

enum open_status open_file(FILE** file, const char* path, const char* user_rights) {
    *file = fopen(path, user_rights);
    if (*file) {
        return OPEN_SUCCESS;
    }

    return OPEN_ERROR;
}

enum close_status close_file(FILE** file) {
    if (!*file) return CLOSE_ERROR;
    fclose(*file);
    return CLOSE_SUCCESS;
}

