#include "bmp_tools.h"
#include "file_tools.h"
#include "transform_tools.h"
#include <stdio.h>

int main( int argc, char** argv ) {
    if (argc != 3) {
        fprintf(stderr, "Количество аргументов должно быть равно 2");
        printf("\n%s", argv[1]);
        return 1;
    }

    FILE *in;
    FILE *out;
    if (open_file(&in, argv[1],"rb") == OPEN_ERROR) {
        fprintf(stderr, "При открытии первого файла возникла ошибка");
        return 1;
    }
    if (open_file(&out, argv[2], "wb") == OPEN_ERROR) {
        fprintf(stderr, "При открытии второго файла возникла ошибка");
        return 1;
    }

    struct image* start_img = NULL;
    if (read_bmp(in,  start_img) == READ_ERROR) {
        fprintf(stderr, "При чтении первого файла возникла ошибка");
        return 1;
    }

    close_file(&in);
    struct image* rotate_img = rotate(start_img);
    free_image(start_img);


    if (write_bmp(out,  rotate_img) == WRITE_ERROR) {
        fprintf(stderr, "При записи второго файла возникла ошибка");
        return 1;
    }

    free_image(rotate_img);
    close_file(&out);

    return 0;
}
