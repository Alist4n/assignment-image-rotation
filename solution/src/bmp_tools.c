#include "bmp_tools.h"

static const uint16_t BMP_SIGNATURE = 19778;
static const uint32_t HEADER_INFO = 40;
static const uint16_t BITS = 24;

static uint32_t get_padding(uint32_t width) {
    if (width % 4 == 0) {
        return 0;
    }
    return 4 - ((width * sizeof(struct pixel)) % 4);
}

static struct bmp_header create_header(const struct image* img) {
    return (struct bmp_header) {
            .bfType = BMP_SIGNATURE,
            .bfileSize = sizeof(struct bmp_header) + sizeof(struct pixel) * img->width * img->height,
            .bfReserved = 0,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = HEADER_INFO,
            .biWidth = img->width,
            .biHeight = img->height,
            .biPlanes = 1,
            .biBitCount = BITS,
            .biCompression = 0,
            .biSizeImage = (img->width * sizeof(struct pixel) + get_padding(img->width)) * img->height,
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0,
    };
}

enum read_status read_bmp(FILE *in, struct image *img) {
    struct bmp_header bmp_header = {0};
    if (read_header(in, &bmp_header) != READ_SUCCESS) {
        return READ_ERROR;
    }

    *image = create_image(bmp_header.biWidth, bmp_header.biHeight);

    const size_t padding = get_bytes_padding(image->width);

    for (size_t i = 0; i < image->height; ++i) {
        for (size_t j = 0; j < image->width; ++j) {
            if (fread(&(image->data[image->width * i + j]), sizeof(struct pixel), 1, in) != 1) {
                return READ_ERROR;
            }
        }
        if (fseek(in, padding, SEEK_CUR) != 0) {
            return READ_ERROR;
        }
    }

    return READ_OK;
}

enum write_status write_bmp(FILE *out, struct image *img) {
    struct bmp_header bmp_header = generate_bmp_header(image);

    if (!fwrite(&bmp_header, sizeof(struct bmp_header), 1, out)) {
        return WRITE_ERROR;
    }

    if (fseek(out, bmp_header.bOffBits, SEEK_SET) != 0) {
        return WRITE_ERROR;
    }

    const uint8_t zero = 0;

    const size_t padding = get_bytes_padding(image->width);

    if (image->data != NULL) {
        for (size_t i = 0; i < image->height; ++i) {
            if (fwrite(image->data + i * image->width, image->width * sizeof(struct pixel), 1, out) < 1) {
                return WRITE_ERROR;
            }
            for (size_t j = 0; j < padding; ++j) {
                if (fwrite(&zero, 1, 1, out) < 1) {
                    return WRITE_ERROR;
                }
            }
        }
    } else {
        return WRITE_ERROR;
    }

    return WRITE_OK;
}

