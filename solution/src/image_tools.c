#include "image_tools.h"

struct image* create_image(uint32_t width, uint32_t height) {
    struct image* new_img = malloc(sizeof(struct image));

    *new_img = (struct image) {
            .width = width,
            .height = height,
            .data = malloc(sizeof (struct pixel) * width * height)
    };

    return new_img;
}

struct pixel* find_pixel_in_image(uint32_t x_coord, uint32_t y_coord, struct image* source) {
    const uint32_t pixel_number = x_coord + y_coord * source->width;
    return &source->data[pixel_number];
}

void free_image(struct image* img) {
    free(img->data);
    free(img);
}
