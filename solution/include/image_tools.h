#ifndef ASSIGNMENT_IMAGE_ROTATION_IMAGE_TOOLS_H
#define ASSIGNMENT_IMAGE_ROTATION_IMAGE_TOOLS_H

#include <malloc.h>
#include <stdint.h>

struct image {
    uint32_t width, height;
    struct pixel *data;
};

struct pixel {
    uint8_t b, g, r;
};

struct image* create_image(uint32_t width, uint32_t height);

void free_image(struct image* img);

struct pixel* find_pixel_in_image(uint32_t x_coord, uint32_t y_coord, struct image* source);

#endif //ASSIGNMENT_IMAGE_ROTATION_IMAGE_TOOLS_H
