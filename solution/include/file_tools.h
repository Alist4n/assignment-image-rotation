
#ifndef ASSIGNMENT_IMAGE_ROTATION_FILE_TOOLS_H
#define ASSIGNMENT_IMAGE_ROTATION_FILE_TOOLS_H

#include <stdio.h>
#include <stdlib.h>

enum open_status {
    OPEN_SUCCESS,
    OPEN_ERROR
};

enum close_status {
    CLOSE_SUCCESS,
    CLOSE_ERROR
};

enum open_status open_file(FILE** file, const char* path, const char* user_rights);

enum close_status close_file(FILE** file);

#endif //ASSIGNMENT_IMAGE_ROTATION_FILE_TOOLS_H
