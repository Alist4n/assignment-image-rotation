#ifndef ASSIGNMENT_IMAGE_ROTATION_TRANSFORM_TOOLS_H
#define ASSIGNMENT_IMAGE_ROTATION_TRANSFORM_TOOLS_H

#include "image_tools.h"

#include <inttypes.h>
#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>

struct image* rotate( struct image* source );

#endif //ASSIGNMENT_IMAGE_ROTATION_TRANSFORM_TOOLS_H
