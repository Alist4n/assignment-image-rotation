#ifndef ASSIGNMENT_IMAGE_ROTATION_BMP_TOOLS_H
#define ASSIGNMENT_IMAGE_ROTATION_BMP_TOOLS_H

#include "image_tools.h"
#include <inttypes.h>
#include <stdint.h>
#include <stdio.h>

struct __attribute__((packed)) bmp_header{
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

enum read_status {
    READ_OK,
    READ_ERROR
};

enum read_status read_bmp(FILE *in, struct image *img);


enum write_status {
    WRITE_OK,
    WRITE_ERROR
};

enum write_status write_bmp(FILE *out, struct image *img);


#endif //ASSIGNMENT_IMAGE_ROTATION_BMP_TOOLS_H
